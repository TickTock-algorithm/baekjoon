#include <stdio.h>
#include <stdlib.h>

int N, M;

void solve(int *buf, int buf_len){
	if(buf_len<M){
		for(int i=buf_len?buf[buf_len-1]:0; i<N; i++){
			buf[buf_len]=i;
			solve(buf, buf_len+1);
		}
	}else{
		for(int i=0; i<M; i++)
			printf("%d ", buf[i]+1);
		puts("");
	}
}

int main(){
	scanf("%d %d", &N, &M);
	int *buf=(int*)malloc(M*sizeof(int));
	solve(buf, 0);
	free(buf);
}