#include <stdio.h>
#include <stdlib.h>

int N, M;

void solve(int *num, int *buf, int buf_len, int sel){
	if(buf_len<M){
		for(int i=0; i<N; i++)
			if(!(sel&(1<<i))){
				buf[buf_len]=num[i];
				solve(num, buf, buf_len+1, sel|(1<<i));
			}
	}else{
		for(int i=0; i<M; i++)
			printf("%d ", buf[i]);
		puts("");
	}
}

int cmp(const void* l, const void* r){
	int a=*(int*)l, b=*(int*)r;
	return a==b?0:(a>b?1:-1);
}

int main(){
	scanf("%d %d", &N, &M);
	
	int *num=(int*)malloc(N*sizeof(int));
	for(int i=0; i<N; i++)
		scanf("%d", num+i);
	qsort(num, N, sizeof(int), cmp);

	int *buf=(int*)malloc(M*sizeof(int));
	
	solve(num, buf, 0, 0);
	
	free(num);
	free(buf);
}