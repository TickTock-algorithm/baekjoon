#include <stdio.h>
#include <stdlib.h>

int N, M;

void solve(int *buf, int buf_len, int sel){
	if(buf_len<M){
		for(int i=0; i<N; i++)
			if(!(sel&(1<<i))){
				buf[buf_len]=i+1;
				solve(buf, buf_len+1, sel|(1<<i));
			}
	}else{
		for(int i=0; i<M; i++)
			printf("%d ", buf[i]);
		puts("");
	}
}

int main(){
	scanf("%d %d", &N, &M);
	int *buf=(int*)malloc(M*sizeof(int));
	solve(buf, 0, 0);
	free(buf);
}