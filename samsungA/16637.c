/**괄호 추가하기
 * https://www.acmicpc.net/problem/16637
 * 0.5 초 (추가 시간 없음) 	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int N;

int calc(int l, int r, char oper){
    switch(oper){
        case '+':
            return l+r;
        case '-':
            return l-r;
        case '*':
            return l*r;
    }
    return 0;
}

int compute(char* eq, int bracket){
    char *t_eq=(char*)malloc(sizeof(char)*N);
    memcpy(t_eq, eq, sizeof(char)*N);

    for(int i=0; i<N; i++){
        if(bracket&(1<<i)){
            t_eq[i-1]=calc(t_eq[i-1], t_eq[i+1], t_eq[i]);
            t_eq[i]='+';
            t_eq[i+1]=0;
        }
    }
    int result=t_eq[0];
    for(int i=1; i<N; i+=2){
        result=calc(result, t_eq[i+1], t_eq[i]);
    }
    free(t_eq);
    return result;
}

int solve(char* eq, int step, int bracket){
    if(step>=N)
        return compute(eq, bracket);
    int tmp1, tmp2;
    tmp1=tmp2=solve(eq, step+2, bracket);
    if(step>2 && !(bracket&(1<<(step-2))))
        tmp2=solve(eq, step+2, bracket|(1<<step));
    return tmp1>tmp2?tmp1:tmp2;
}

int main(){
    scanf("%d\n", &N);
    char *eq=(char*)malloc(sizeof(char)*N);
    for(int i=0; i<N; i++){
        scanf("%c", eq+i);
        if(eq[i]>='0' && eq[i]<='9')
            eq[i]-='0';
    }
    printf("%d\n", solve(eq,1, 0));
    free(eq);
    return 0;
}