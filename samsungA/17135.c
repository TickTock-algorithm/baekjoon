/**캐슬 디펜스
 * https://www.acmicpc.net/problem/17135
 * 1 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#define ARCHER_NUM 3

int N, M, D;

struct pos{
    int r,c;
};

int play(int *map, int sel){
    int sum=0;
    for(int t=N; t>0; t--){
        struct pos target[ARCHER_NUM];        
        for(int i=0, archer=0; archer<ARCHER_NUM; i++){
            if((sel>>i)&1){
                int min=D+1;
                target[archer].r=target[archer].c=-1;

                for(int c=0; c<M; c++){
                    for(int r=0; r<t; r++){
                        if(map[r*M+c]){
                            int d=abs(r-t)+abs(c-i);
                            if(d<min){
                                min=d;
                                target[archer].r=r;
                                target[archer].c=c;
                            }
                        }
                    }
                }

                archer++;
            }
        }
        for(int i=0; i<ARCHER_NUM; i++)
            if(target[i].r>=0 && map[target[i].r*M+target[i].c]){
                sum++;
                map[target[i].r*M+target[i].c]=0;
            }
    }
    return sum;
}

int select_archer(int *map, int num, int idx, int sel){
    if(ARCHER_NUM-num > M-idx)
        return 0;
    int ret, buf;
    if(num>=ARCHER_NUM){
        int *copy_map=(int*)malloc(N*M*sizeof(int));
        memcpy(copy_map, map, N*M*sizeof(int));
        ret=play(copy_map, sel);
        free(copy_map);
        return ret;
    }
    ret=select_archer(map, num+1, idx+1, sel|(1<<idx));
    buf=select_archer(map, num, idx+1, sel);
    return ret>buf?ret:buf;
}

int solve(int* map){
    return select_archer(map, 0, 0, 0);
}

int main(){
    scanf("%d %d %d", &N, &M, &D);
    int *map=(int*)malloc(N*M*sizeof(int));
    for(int i=0; i<N*M; i++)
        scanf("%d", map+i);
    printf("%d\n", solve(map));
    free(map);
    return 0;
}