/**파이프 옮기기 1
 * https://www.acmicpc.net/problem/17070
 * 1 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int N;

int func(int *map, int state, int r, int c){
    if(r==N || c==N)
        return 0;
    if(r==N-1 && c==N-1)
        return 1;
    int result=0;
    if(!map[r*N+c+1])
        if(state==0 || state==2)
            result+=func(map, 0, r, c+1);
    if(!map[(r+1)*N+c])
        if(state==1 || state==2)
            result+=func(map, 1, r+1, c);
    if(!map[r*N+c+1] && !map[(r+1)*N+c] && !map[(r+1)*N+c+1])
        result+=func(map, 2, r+1, c+1);
    return result;
}

int solve(int *map){
    return func(map, 0, 0, 1);
}

int main(){
    scanf("%d", &N);
    int *map=(int*)malloc((N+1)*(N+1)*sizeof(int));
    memset(map, 1, (N+1)*(N+1)*sizeof(int));
    for(int r=0; r<N; r++)
        for(int c=0; c<N; c++)
            scanf("%d", map+r*N+c);
    printf("%d\n", solve(map));
    free(map);
    return 0;
}