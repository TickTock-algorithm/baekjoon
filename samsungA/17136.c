/**색종이 붙이기
 * https://www.acmicpc.net/problem/17136
 * 1 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define N 10
#define SIZE 5
#define MAX 5

int RULER[SIZE];
int CLEAR_MAP[N][N];

int func(int map[N][N], int paper[SIZE], int sr, int count, int min){
    if(count>=min)
        return min;
    for(int r=sr; r<N; r++){
        for(int c=0; c<N; c++){
            if(!map[r][c])
                continue;
            int t=(N-((r>c)?r:c));
            for(int n=SIZE<t?SIZE:t; n>0; n--){
                int is_square=1;
                for(int i=0; i<n; i++)
                    if(memcmp(&map[r+i][c], RULER, n*sizeof(int))){
                        is_square=0;
                        break;
                    }
                
                if(is_square){
                    if(paper[n-1]>=MAX)
                        continue;
                    paper[n-1]++;
                    for(int i=0; i<n; i++)
                        memset(&map[r+i][c], 0, n*sizeof(int));
                    if(!memcmp(map, CLEAR_MAP, N*N*sizeof(int))){
                        paper[n-1]--;
                        for(int i=0; i<n; i++)
                            memcpy(&map[r+i][c], RULER, n*sizeof(int));
                        return count+1;
                    }
                    
                    int tmp=func(map, paper, r, count+1, min);
                    min=min>tmp?tmp:min;
                    paper[n-1]--;
                    for(int i=0; i<n; i++)
                        memcpy(&map[r+i][c], RULER, n*sizeof(int));
                }
            }
            return min;
        }
    }
    return min;
}

int solve(int map[N][N]){
    if(!memcmp(map, CLEAR_MAP, N*N*sizeof(int)))
        return 0;

    for(int i=0; i<SIZE; i++)
        RULER[i]=1;
    memset(CLEAR_MAP, 0, N*N*sizeof(int));

    int paper[SIZE];
    memset(paper, 0, MAX*sizeof(int));

    int result=func(map, paper, 0, 0, ~0u>>1);
    return result>SIZE*MAX?-1:result;
}

int main(){
    int map[N][N];
    for(int r=0; r<N; r++)
        for(int c=0; c<N; c++)
            scanf("%d", &map[r][c]);
    printf("%d\n", solve(map));
    return 0;
}