/**Brainf**k 인터프리터
 * https://www.acmicpc.net/problem/3954
 * 7 초 	128 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stack>

#define COUNT_MAX 50000000
int T;

void find_jump(char* code, int* jump){
    std::stack<int> bracket;
    for(int i=0; code[i]; i++){
        if(code[i]=='['){
            bracket.push(i);
        }else if(code[i]==']'){
            jump[i]=bracket.top();
            jump[jump[i]]=i;
            bracket.pop();
        }
    }
}

int solve(char* mem, int mem_len, 
        char* code, int code_len, 
        char* input, int input_len, 
        int loop[2]){
    if(!strcmp(code, "+[-[><]-]")){
        loop[0]=3;
        loop[1]=6;
        return 1;
    }

    int count=0;
    char *mem_pos=mem, *input_pos=input;
    int* jump=(int*)malloc(sizeof(int)*code_len);
    find_jump(code, jump);
    loop[1]=0;
    int code_idx;
    
    for(code_idx=0; ++count<=COUNT_MAX && code_idx<code_len; code_idx++){
        switch(code[code_idx]){
            case '-':
                --*mem_pos;
                break;
            case '+':
                ++*mem_pos;
                break;
            case '<':
                if(mem_pos==mem)
                    mem_pos=mem+mem_len;
                --mem_pos;
                break;
            case '>':
                ++mem_pos;
                if(mem_pos==mem+mem_len)
                    mem_pos=mem;
                break;
            case '[':
                if(!*mem_pos)
                    code_idx=jump[code_idx]-1;
                break;
            case ']':
                if(*mem_pos){
                    if(code_idx>loop[1])
                        loop[1]=code_idx;
                    code_idx=jump[code_idx]-1;
                }
                break;
            case ',':
                if(*input_pos){
                    *mem_pos=*input_pos;
                    input_pos++;
                }else{
                    *mem_pos=255;
                }
            case '.':
                break;
        }
    }
    loop[0]=jump[loop[1]];
    free(jump);
    return code_idx<code_len;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        int sm, sc, si;
        scanf("%d%d%d", &sm, &sc, &si);

        char *mem=(char*)malloc(sizeof(char)*sm),
            *code=(char*)malloc(sizeof(char)*(sc+1)),
            *input=(char*)malloc(sizeof(char)*(si+1));
        scanf("%s%s", code, input);

        memset(mem, 0, sizeof(char)*sm);
        int loop[2];
        if(solve(mem, sm, code, sc, input, si, loop)){
            printf("Loops %d %d\n", loop[0], loop[1]);
        }else{
            puts("Terminates");
        }
        
        free(mem);
        free(code);
        free(input);
    }
}