/**세 번 뒤집기
 * https://www.acmicpc.net/problem/2561
 * 1 초	128 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#define MAX_STEP 3
#define DIV_LEN 32

int N;

int comp(const void *l, const void *r){
    return (*(int*)l)>(*(int*)r);
}

int check(int *num){
    for(int i=0; i<N; i++)
        if(num[i]!=i+1)
            return 0;
    return 1;
}

int getDiv(int *num, int *div, int *len){
    *len=0;
    int before=-1;
    for(int i=0; i<N; i++){
        if(abs(num[i]-before)!=1){
            // if(i>0 && div[(*len)-1]<i-1)
            //     div[(*len)++]=i-1;
            // if(i==0 || div[(*len)-1]<i)
                div[(*len)++]=i;
            // if(i<N-1)
                // div[(*len)++]=i+1;
        }
        before=num[i];
    }
    // if(div[(*len)-1]!=N)
        div[(*len)++]=N;
    
    for(int i=0, tlen=*len; i<tlen; i++){
        if(i<tlen-1 && abs(num[div[i]]-num[div[i+1]])){
            div[(*len)++]=i+1;
        }
        if(div[i]>1 && i<tlen-1 && num[div[i]-2]-num[div[i+1]-1]){
            div[(*len)++]=i+1;
        }
    }

    div[(*len)++]=1;
    qsort(div, *len, sizeof(int), comp);
    for(int i=0; i<*len; i++)
        printf("%d ", div[i]);
        puts("");
    exit(0);
    return *len;
}

int* flip(int *num, int l, int r){
    while(l<r){
        r--;
        int t=num[l];
        num[l]=num[r];
        num[r]=t;
        l++;
    }
    return num;
}

int* solve(int *num, int step){
    if(step<=0)
        return NULL;
    

    int *div=(int*)malloc(sizeof(int)*DIV_LEN);
    int div_len;
    getDiv(num, div, &div_len);

    int *result=NULL;
    int *tmp_num=(int*)malloc(sizeof(int)*N);
    int l, r;
    for(l=0; l<div_len; l++){
        for(r=l+1; r<div_len; r++){
            memcpy(tmp_num, num, sizeof(int)*N);
            
            if(check(flip(tmp_num, div[l], div[r]))){
                result=(int*)malloc(sizeof(int)*MAX_STEP*2);
                memset(result, 0, sizeof(int)*MAX_STEP*2);
                goto RETURN;
            }else if(result=solve(tmp_num, step-1)){
                goto RETURN;
            }
        }
    }
    
    RETURN:
    if(result){
        result[(MAX_STEP-step)*2]=div[l]+1;
        result[(MAX_STEP-step)*2+1]=div[r];
    }
    free(tmp_num);
    free(div);
    return result;
}

int main(){
    scanf("%d", &N);
    int *num=(int*)malloc(sizeof(int)*N);
    for(int i=0; i<N; i++)
        scanf("%d", num+i);

    int *result=solve(num, MAX_STEP);
    for(int i=0; i<MAX_STEP; i++)
        printf("%d %d\n", result[i*2], result[i*2+1]);

    free(num);
    free(result);
    return 0;
}