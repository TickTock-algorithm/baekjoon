/**Gaaaaaaaaaarden
 * https://www.acmicpc.net/problem/18809
 * 2 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX_GROUND 10

#define RIVER 0
#define IMPOSSIBLE (1<<0)
#define POSSIBLE (1<<1)

#define GREEN (1<<2)
#define RED (1<<3)
#define FLOWER (1<<4)

int N, M, G, R;

int spread(int *base, int move){
    int *p=base+move;
    if((*p)&(POSSIBLE|IMPOSSIBLE)){
        int ret=(*p)&(GREEN|RED);
        *p|=*base;
        return !ret;
    }
    return 0;
}

int run(int* map, int** ground){
    int conti=0, count=0;
    int *t_map=(int*)malloc(sizeof(int)*N*M);
    memcpy(t_map, map, sizeof(int)*N*M);

    int **queue=(int**)malloc(sizeof(int*)*N*M), queue_head=0, queue_tail=0;
    for(int i=0; i<MAX_GROUND && ground[i]; i++)
        if(*ground[i]&(GREEN|RED))
            queue[queue_tail++]=t_map+(ground[i]-map);

    do{
        conti=0;
        int last=queue_tail;
        for(int i=queue_head; i<last; i++){
            int *p=queue[i];

            if(*p==FLOWER)
                continue;

            int r=(p-t_map)/M, c=(p-t_map)%M;

            if(r>0 && spread(p, -M)){
                queue[queue_tail++]=p-M;
                conti|=*p;
            }
            if(r<N-1 && spread(p, M)){
                queue[queue_tail++]=p+M;
                conti|=*p;
            }
            if(c>0 && spread(p, -1)){
                queue[queue_tail++]=p-1;
                conti|=*p;
            }
            if(c<M-1 && spread(p, 1)){
                queue[queue_tail++]=p+1;
                conti|=*p;
            }
        }
        
        queue_head=last;
        for(int i=queue_head; i<queue_tail; i++){
            int *p=queue[i];
            *p&=~POSSIBLE&~IMPOSSIBLE;
            if(!(*p^GREEN^RED)){
                *p=FLOWER;
                count++;
            }
        }
    }while(!(conti^GREEN^RED));

    free(t_map);
    free(queue);
    
    return count;
}

int solve(int* map, int** ground, int rest, int green_count, int red_count){
    if(!green_count && !red_count)
        return run(map, ground);
    if(rest<=0 || rest<green_count+red_count)
        return 0;
    
    int max=0, tmp;
    
    if(green_count>=0){
        *ground[rest-1]=GREEN;
        tmp=solve(map, ground, rest-1 ,green_count-1, red_count);
        max=max>tmp?max:tmp;
    }
    
    if(red_count>=0){
        *ground[rest-1]=RED;
        tmp=solve(map, ground, rest-1 ,green_count, red_count-1);
        max=max>tmp?max:tmp;
    }
    
    *ground[rest-1]=POSSIBLE;
    tmp=solve(map, ground, rest-1 ,green_count, red_count);
    max=max>tmp?max:tmp;

    return max;
}

int main(){
    scanf("%d %d %d %d", &N, &M, &G, &R);

    int *map=(int*)malloc(sizeof(int)*N*M);
    int **ground=(int**)malloc(sizeof(int*)*MAX_GROUND);
    memset(ground, 0, sizeof(int*)*MAX_GROUND);
    int ground_idx=0;
    for(int i=0; i<N*M; i++){
        scanf("%d", map+i);
        if(map[i]==POSSIBLE)
            ground[ground_idx++]=map+i;
    }
    
    printf("%d\n", solve(map, ground, ground_idx, G, R));

    free(ground);
    free(map);
    return 0;
}