/**퇴사
 * https://www.acmicpc.net/problem/14501
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int N;
struct work {
    int day;
    int money;
} * work;

int calc(int day) {
    int max = 0, res;
    for (int i = day; i < N; i++) {
        if (i + work[i].day <= N) {
            res = calc(i + work[i].day) + work[i].money;
            max = max < res ? res : max;
        }
    }
    return max;
}

int main() {
    scanf("%d", &N);
    work = (struct work*)malloc(sizeof(struct work) * N);
    for (int i = 0; i < N; i++) scanf("%d %d", &work[i].day, &work[i].money);
    printf("%d\n", calc(0));
}