/**째로탈출 2
 * https://www.acmicpc.net/problem/13460
 * 2 초 	512 MB
 */

#include<stdio.h>
#include<stdlib.h>

int N, M;

void printMap(char* map, char* rp, char* bp){
	*rp='R';
	*bp='B';
	puts("");
	for(int i=0; i<M; i++){
		for(int g=0; g<N; g++)
			printf("%c", *(map+i*M+g));
		puts("");
	}
	*rp='.';
	*bp='.';
}

int move(char *map, char* rp, char* bp, int step, char *buf[2]){
	char *trp=rp, *tbp=bp;
	int rp_check=1, bp_check=1;
	int ret=0;
	do{
		if(rp && rp_check){
			rp+=step;
			if(rp==bp || *rp=='#'){
				rp-=step;
				rp_check=0;
				continue;
			}else if(*rp=='O')
				rp=NULL;
			bp_check=1;
		}
		if(bp && bp_check){
			bp+=step;
			if(rp==bp || *bp=='#'){
				bp-=step;
				bp_check=0;
				continue;
			}else if(*bp=='O')
				bp=NULL;
			rp_check=1;
		}
	}while( (rp && rp_check) || (bp && bp_check));
	if(!bp)
		return -1;
	else if(!rp)
		return 1;
	if(trp==rp && tbp == bp)
		return -1;
	buf[0]=rp;
	buf[1]=bp;
	return 0;
}

int func(char* map, char* rp, char* bp, int stage){
	if(++stage>10)
		return 11;
	char *buf[4][2];
	int min=11, tmp;
	int check=0;
	for(int i=0; i<4; i++){
		switch(move(map, rp, bp, ((i/2)?1:-1)*((i%2)?1:M), buf[i])){
			case 1:
				return stage;
			case 0:
				break;
			case -1:
				check|=1<<i;
				break;
		}
	}
	for(int i=0; i<4; i++){
		if(check&(1<<i))
			continue;
		tmp=func(map, buf[i][0], buf[i][1], stage);
		min=min<tmp?min:tmp;
	}
	
	return min;
}

int main(){
	scanf("%d %d", &N, &M);
	char *map=(char*)malloc(sizeof(char)*N*M);
	char *rp, *bp;
	for(int i=0; i<N*M; i++){
		scanf("%c", map+i);
		switch(*(map+i)){
			case 'R':
				rp=map+i;
				*(map+i)='.';
				break;
			case 'B':
				bp=map+i;
				*(map+i)='.';
				break;
			case '#': case '.': case 'O':
				break;
			default:
				i--;
				continue;
		}
	}
	int result=func(map, rp, bp, 0);
	printf("%d\n", result>10?-1:result);
}