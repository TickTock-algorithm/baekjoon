/**톱니바퀴
 * https://www.acmicpc.net/problem/14891
 * 2 초 	512 MB
 */

#include <stdio.h>

#define GEAR_COUNT 4
#define GEAR_TOOTH 8

int gear[GEAR_COUNT] = {
    0,
};

int check_rotate(int num, int step) {
    return !step ||
           (!!(gear[num] &
               (1 << ((GEAR_TOOTH / 4 * -step + GEAR_TOOTH) % GEAR_TOOTH))) ^
            !!(gear[num - step] &
               (1 << ((GEAR_TOOTH / 4 * step + GEAR_TOOTH) % GEAR_TOOTH))));
}
void rotate(int num, int direct, int step) {
    if (num < 0 || num >= GEAR_COUNT) return;

    if (check_rotate(num, step)) {
        if (!step) {
            rotate(num + 1, !direct, 1);
            rotate(num - 1, !direct, -1);
        } else
            rotate(num + step, !direct, step);

        if (direct) {
            gear[num] <<= 1;
            if (gear[num] & (1 << GEAR_TOOTH))
                gear[num] -= (1 << GEAR_TOOTH) - 1;
        } else {
            if (gear[num] & 1) gear[num] |= 1 << GEAR_TOOTH;
            gear[num] >>= 1;
        }
    }
}

int main() {
    // 입력
    for (int i = 0; i < GEAR_COUNT; i++) {
        char input[GEAR_TOOTH + 1];
        scanf("%s", input);
        for (int g = GEAR_TOOTH - 1; g >= 0; g--) {
            gear[i] |= (input[g] == '1') ? 1 : 0;
            gear[i] <<= 1;
        }
        gear[i] >>= 1;
    }
    // 회전
    int rcount;
    scanf("%d", &rcount);
    for (int i = 0; i < rcount; i++) {
        int gear_num, direct;
        scanf("%d %d", &gear_num, &direct);
        rotate(gear_num - 1, direct + 1, 0);
    }
    // 결과
    int result = 0;
    for (int i = GEAR_COUNT - 1; i >= 0; i--) {
        result |= gear[i] & 1;
        result <<= 1;
    }
    printf("%d\n", result >>= 1);

    return 0;
}