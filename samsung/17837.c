#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define RED 1
#define BLUE 2

int N, K;
const int MOV[][2]={{0, 1}, {0, -1}, {-1, 0}, {1, 0}};
const int REVERSE[]={1, 0, 3, 2};

struct horse{
    int r, c, d;
};

struct cell{
    int type;
    int slen;
    struct horse *stack[3];
};

int move(struct cell* map, struct horse* h, int nr, int nc){
    struct cell* src=map+h->r*N+h->c;
    int hi=-1;
    for(int i=0; i<src->slen; i++){
        if(h==src->stack[i]){
            hi=i;
        }
        if(hi>=0){
            src->stack[i]->r=nr;
            src->stack[i]->c=nc;
        }
    }
    struct cell* dst=map+nr*N+nc;
    int tlen=src->slen-hi;
    if(dst->slen+tlen>=4)
        return 1;
    memcpy(dst->stack+dst->slen, src->stack+hi, sizeof(struct horse*)*tlen);
    src->slen-=tlen;
    dst->slen+=tlen;
    return 0;
}

void reverse(struct cell* cell, struct horse* h){
    int hi;
    for(int i=0; i<cell->slen; i++){
        if(h==cell->stack[i]){
            hi=i;
            break;
        }
    }
    int tlen=cell->slen-hi;
    struct horse *tmp[3];
    memcpy(tmp, cell->stack+hi, sizeof(struct horse*)*tlen);
    for(int i=tlen-1; i>=0; i--)
        cell->stack[hi++]=tmp[i];
}

int solve(struct cell* map, struct horse* horse){
    int turn=1;
    for(; turn<=1000; turn++){
        for(int k=0; k<K; k++){
            struct horse *h=horse+k;
            int nr=h->r+MOV[h->d][0],
                nc=h->c+MOV[h->d][1];
            if(nr<0 || nr>=N || nc<0 || nc>=N || map[nr*N+nc].type==BLUE){
                h->d=REVERSE[h->d];

                nr=h->r+MOV[h->d][0];
                nc=h->c+MOV[h->d][1];
                if(nr<0 || nr>=N || nc<0 || nc>=N || map[nr*N+nc].type==BLUE)
                    continue;
            }
            
            if(move(map, h, nr, nc))
                return turn;

            if(map[nr*N+nc].type==RED)
                reverse(map+nr*N+nc, h);
        }
    }
    return turn>1000?-1:turn;
}

int main(){
    scanf("%d %d", &N, &K);

    struct cell *map=(struct cell*)malloc(sizeof(struct cell)*N*N);
    for(int i=0; i<N*N; i++){
        scanf("%d", &map[i].type);
        map[i].slen=0;
    }
    
    struct horse *horse=(struct horse*)malloc(sizeof(struct horse)*K);
    for(int i=0; i<K; i++){
        scanf("%d %d %d", &horse[i].r, &horse[i].c, &horse[i].d);
        horse[i].r--;
        horse[i].c--;
        horse[i].d--;
        struct cell* cell=map+horse[i].r*N+horse[i].c;
        cell->stack[cell->slen++]=horse+i;
    }

    printf("%d\n", solve(map, horse));

    free(map);
    free(horse);
    return 0;
}