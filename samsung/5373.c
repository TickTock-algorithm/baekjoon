/**큐빙
 * https://www.acmicpc.net/problem/5373
 * 1 초 	128 MB
 */

#include<stdio.h>
#include<string.h>
/*
  3
4 0 5 1
  2

012
345
678
*/
int MOVE_BASE[9]={2, 5, 8, 1, 4, 7, 0, 3, 6};
int MOVE_SIDE[6][12]={
    {3*9+6, 3*9+7, 3*9+8, 5*9+0, 5*9+3, 5*9+6, 2*9+2, 2*9+1, 2*9+0, 4*9+8, 4*9+5, 4*9+2},
    {3*9+2, 3*9+1, 3*9+0, 4*9+0, 4*9+3, 4*9+6, 2*9+6, 2*9+7, 2*9+8, 5*9+8, 5*9+5, 5*9+2},
    {0*9+6, 0*9+7, 0*9+8, 5*9+6, 5*9+7, 5*9+8, 1*9+6, 1*9+7, 1*9+8, 4*9+6, 4*9+7, 4*9+8},
    {1*9+2, 1*9+1, 1*9+0, 5*9+2, 5*9+1, 5*9+0, 0*9+2, 0*9+1, 0*9+0, 4*9+2, 4*9+1, 4*9+0},
    {3*9+0, 3*9+3, 3*9+6, 0*9+0, 0*9+3, 0*9+6, 2*9+0, 2*9+3, 2*9+6, 1*9+8, 1*9+5, 1*9+2},
    {3*9+8, 3*9+5, 3*9+2, 1*9+0, 1*9+3, 1*9+6, 2*9+8, 2*9+5, 2*9+2, 0*9+8, 0*9+5, 0*9+2},
};

void printCube(char cube[6][9]){
    char *space="   ";
    printf("%s%c%c%c\n", space, cube[3][0], cube[3][1], cube[3][2]);
    printf("%s%c%c%c\n", space, cube[3][3], cube[3][4], cube[3][5]);
    printf("%s%c%c%c\n", space, cube[3][6], cube[3][7], cube[3][8]);
    printf("%c%c%c%c%c%c%c%c%c%c%c%c\n", cube[4][0], cube[4][1], cube[4][2], cube[0][0], cube[0][1], cube[0][2], cube[5][0], cube[5][1], cube[5][2], cube[1][0], cube[1][1], cube[1][2]);
    printf("%c%c%c%c%c%c%c%c%c%c%c%c\n", cube[4][3], cube[4][4], cube[4][5], cube[0][3], cube[0][4], cube[0][5], cube[5][3], cube[5][4], cube[5][5], cube[1][3], cube[1][4], cube[1][5]);
    printf("%c%c%c%c%c%c%c%c%c%c%c%c\n", cube[4][6], cube[4][7], cube[4][8], cube[0][6], cube[0][7], cube[0][8], cube[5][6], cube[5][7], cube[5][8], cube[1][6], cube[1][7], cube[1][8]);
    printf("%s%c%c%c\n", space, cube[2][0], cube[2][1], cube[2][2]);
    printf("%s%c%c%c\n", space, cube[2][3], cube[2][4], cube[2][5]);
    printf("%s%c%c%c\n", space, cube[2][6], cube[2][7], cube[2][8]);
}

void rotate(char cube[6][9], int s, int rot){
    char copy_cube[6][9];
    
    for(int i=0; i<rot; i++){
        memcpy(copy_cube, cube, 6*9*sizeof(char));
        for(int i=0; i<9; i++){
            cube[s][MOVE_BASE[i]]=copy_cube[s][i];
        }
        for(int i=0; i<12; i++){
            *((char*)cube+MOVE_SIDE[s][(i+3)%12])=*((char*)copy_cube+MOVE_SIDE[s][i]);
        }
    }
}
void initCube(char cube[6][9]){
    memset(cube[0], 'w', 9);
    memset(cube[1], 'y', 9);
    memset(cube[2], 'r', 9);
    memset(cube[3], 'o', 9);
    memset(cube[4], 'g', 9);
    memset(cube[5], 'b', 9);
}
void printResult(char cube[6][9]){
    for(int i=0; i<9; i++){
        printf("%c", cube[0][i]);
        if((i+1)%3==0)
            puts("");
    }
}
int main(){
    int T;
    scanf("%d", &T);
    for(int tc=0; tc<T; tc++){
        char cube[6][9];    
        initCube(cube);
        int n;
        scanf("%d", &n);
        char buf[3];
        for(int i=0; i<n; i++){
            scanf("%s", buf);
            int square=0;
            switch(buf[0]){
                case 'R': square++;
                case 'L': square++;
                case 'B': square++;
                case 'F': square++;
                case 'D': square++;
            }
            rotate(cube, square, buf[1]=='+'?1:3);
        }
        printResult(cube);
    }
}