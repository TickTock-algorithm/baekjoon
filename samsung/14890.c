/**경사로
 * https://www.acmicpc.net/problem/14890
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int *map, N, L;

int check(int* pos, int step) {
    int count = 1;
    for (int i = 0; i < N - 1; i++) {
        switch (pos[i * step] - pos[(i + 1) * step]) {
            case 0:
                count++;
                break;
            case 1:
                count = 1;
                for (int g = 0; g < L; g++)
                    if ((i + g + 2 < N) &&
                        (pos[(i + g + 1) * step] == pos[(i + g + 2) * step]))
                        count++;
                    else
                        break;
                if (count < L) return 0;
                i += L - 1;
                count = 0;
                break;
            case -1:
                if (count < L) return 0;
                count = 1;
                break;
            default:
                return 0;
        }
    }
    return 1;
}

int main() {
    scanf("%d %d", &N, &L);
    map = (int*)malloc(sizeof(int) * N * N);
    for (int i = 0; i < N * N; i++) scanf("%d", map + i);

    int result = 0;
    for (int i = 0; i < N; i++) {
        result += check(map + i * N, 1);
        result += check(map + i, N);
    }
    printf("%d\n", result);
}