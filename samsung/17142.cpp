/**연구소 3
 * https://www.acmicpc.net/problem/17142
 * 0.25 초 (하단 참고) 	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<vector>

using namespace std;

int N, M;

void printMap(int *map){
    for(int r=0; r<N; r++){
        for(int c=0; c<N; c++){
            printf("%d ", map[r*N+c]);
        }
        puts("");
    }
    puts("");
}
int check(int *map){
    for(int i=0; i<N*N; i++)
        if(!map[i])
            return 0;
    return 1;
}

int spread(int* map, int time){
    int flag=0;
    for(int r=0; r<N; r++){
        for(int c=0; c<N; c++){
            int *p=map+r*N+c;
            if(*p==-time){
                if(r>0 && !*(p-N))
                    flag=*(p-N)=-time-1;
                if(c>0 && !*(p-1))
                    flag=*(p-1)=-time-1;
                if(r<N-1 && !*(p+N))
                    flag=*(p+N)=-time-1;
                if(c<N-1 && !*(p+1))
                    flag=*(p+1)=-time-1;
            }
        }
    }
    return flag;
}

void makeComb(int sel, int step, int remain, int max, vector<int> &comb){
    if(!remain){
        comb.push_back(sel);
        return;
    }
    if(step>=max)
        return;
    
    makeComb(sel, step+1, remain, max, comb);
    makeComb(sel|(1<<step), step+1, remain-1, max, comb);
}

int solve(int *map, vector<int*> &vir){
    int min=~0u>>1;
    vector<int> comb;
    makeComb(0, 0, M, vir.size(), comb);
    int *backup=(int*)malloc(N*N*sizeof(int));
    memcpy(backup, map, N*N*sizeof(int));
    for(int i=0; i<comb.size(); i++){
        memcpy(map, backup, N*N*sizeof(int));
        for(int g=0; g<vir.size(); g++){
            if(comb[i]&(1<<g)){
                *vir[g]=-1;
            }
        }
        int time=1;
        while(spread(map, time))
            if(time>=min)
                break;
            else
                time++;
        if(check(map)){
            // printf("%d\n", comb[i]);
            // printMap(map);
            min=min<time?min:time;
        }
    }
    free(backup);
    return (min==~0u>>1)?-1:min-1;
}

int main(){
    scanf("%d %d", &N, &M);
    
    int *map=(int*)malloc(N*N*sizeof(int));

    vector<int*> vir;
    for(int i=0; i<N*N; i++){
        scanf("%d", map+i);
        if(map[i]==2)
            vir.push_back(map+i);
    }

    printf("%d\n", solve(map, vir));

    free(map);
    return 0;
}