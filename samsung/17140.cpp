/**이차원 배열과 연산
 * https://www.acmicpc.net/problem/17140
 * 0.5 초 (추가 시간 없음) 	512 MB
 */

#include<stdio.h>
#include<string.h>

#include<algorithm>

#include<vector>
#include<utility>
#include<map>

using namespace std;

int R, C, K;

int arr[1000][1000];

void printArr(int mr, int mc){
    for(int r=0; r<mr; r++){
        for(int c=0; c<mc; c++){
            printf("%d ", arr[r][c]);
        }
        puts("");
    }
    puts("");
}

vector<pair<int, int>>* customSort(map<int, int> &data){
    vector<pair<int, int>> *output=new vector<pair<int, int>>();
    for(map<int,int>::iterator iter=data.begin(); iter!=data.end(); iter++){
        output->push_back(make_pair(iter->first, iter->second));
    }
    sort(output->begin(), output->end(), [](const pair<int,int>& l, const pair<int,int>& r){
        if(l.second<r.second)
            return true;
        if(l.second<r.second)
            return false;
        if(l.first<r.first)
            return true;
        return false;
    });
    return output;
}

int solve(){
    int mr=3, mc=3;
    for(int t=0; t<100; t++){
        if(arr[R][C]==K)
            return t;
            
        map<int, int> data;

        if(mr<mc){
            // C연산
            int nr=0;

            for(int c=0; c<mc; c++){
                data.clear();
                for(int r=0; r<mr; r++){
                    if(arr[r][c]==0)
                        continue;
                    data[arr[r][c]]++;
                }
                vector<pair<int, int>> *ret=customSort(data);
                nr=nr>ret->size()?nr:ret->size();
                for(int r=0; r<mr; r++)
                    arr[r][c]=0;
                for(int r=0; r<ret->size(); r++){
                    arr[r*2][c]=ret->at(r).first;
                    arr[r*2+1][c]=ret->at(r).second;
                }
                delete ret;
            }
            mr=nr*2;
        }else{
            // R연산
            int nc=0;
            for(int r=0; r<mr; r++){
                data.clear();
                for(int c=0; c<mc; c++){
                    if(arr[r][c]==0)
                        continue;
                    data[arr[r][c]]++;
                }
                vector<pair<int, int>> *ret=customSort(data);
                nc=nc>ret->size()?nc:ret->size();
                for(int c=0; c<mc; c++)
                    arr[r][c]=0;
                for(int c=0; c<ret->size(); c++){
                    arr[r][c*2]=ret->at(c).first;
                    arr[r][c*2+1]=ret->at(c).second;
                }
                delete ret;
            }
            mc=nc*2;
        }
        printArr(mr, mc);
    }
    
    return -1;
}

int main(){
    scanf("%d %d %d", &R, &C, &K);
    R--;
    C--;

    memset(arr, 0, sizeof(arr));
    for(int r=0; r<3; r++){
        for(int c=0; c<3; c++){
            scanf("%d", &arr[r][c]);
        }
    }
    
    printf("%d\n", solve());

    return 0;
}