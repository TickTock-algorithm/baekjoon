/**미세먼지 안녕!
 * https://www.acmicpc.net/problem/17144
 * 1 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<vector>

using namespace std;

int R, C, T;

void printMap(int* map){
    for(int r=0; r<R; r++){
        for(int c=0; c<C; c++)
            printf("%d ", map[r*C+c]);
        puts("");
    }
}

void setDustPath(int* map, vector<int*> *dust_path){
    for(int i=1; i<R; i++){
        if(map[i*C]==-1){
            for(int g=1; g<C; g++){
                dust_path[0].push_back(map+i*C+g);
                dust_path[1].push_back(map+(i+1)*C+g);
            }
            for(int g=i; g>0; g--){
                dust_path[0].push_back(map+g*C-1);
            }
            for(int g=C-2; g>=0; g--){
                dust_path[0].push_back(map+g);
            }
            for(int g=1; g<i; g++){
                dust_path[0].push_back(map+g*C);
            }
            for(int g=i+3; g<=R; g++){
                dust_path[1].push_back(map+g*C-1);
            }
            for(int g=2; g<C; g++){
                dust_path[1].push_back(map+C*R-g);
            }
            for(int g=R-1; g>i+1; g--){
                dust_path[1].push_back(map+g*C);
            }
            break;
        }
    }
}

void spreadDust(int* map){
    int *tmp_map=(int*)malloc(R*C*sizeof(int));
    memset(tmp_map, 0, R*C*sizeof(int));

    for(int r=0; r<R; r++){
        for(int c=0; c<C; c++){
            int count=0;
            int *p=map+r*C+c, *tp=tmp_map+r*C+c;
            if(r>0 && *(p-C)>=0){
                *(tp-C)+=*p/5;
                count++;
            }
            if(c>0 && *(p-1)>=0){
                *(tp-1)+=*p/5;
                count++;
            }
            if(r<R-1 && *(p+C)>=0){
                *(tp+C)+=*p/5;
                count++;
            }
            if(c<C-1 && *(p+1)>=0){
                *(tp+1)+=*p/5;
                count++;
            }
            *p-=*p/5*count;
        }
    }
    for(int r=0; r<R; r++){
        for(int c=0; c<C; c++){
            map[r*C+c]+=tmp_map[r*C+c];
        }
    }
    free(tmp_map);
}

void cleanDust(vector<int*> *dust_path){
    for(int d=0; d<2; d++){
        for(int i=dust_path[d].size()-1; i>0; i--)
            *dust_path[d][i]=*dust_path[d][i-1];
        *dust_path[d][0]=0;
    }
}
int countDust(int* map){
    int sum=0;
    for(int r=0; r<R; r++){
        for(int c=0; c<C; c++){
            if(map[r*C+c]>0)
                sum+=map[r*C+c];
        }
    }
    return sum;
}
int main(){
    scanf("%d %d %d", &R, &C, &T);
    
    int *map=(int*)malloc(R*C*sizeof(int));
    for(int i=0; i<R*C; i++){
        scanf("%d", map+i);
    }
    
    vector<int*> dust_path[2];
    setDustPath(map, dust_path);
    
    for(int i=0; i<T; i++){
        spreadDust(map);
        cleanDust(dust_path);
    }
    
    printf("%d\n", countDust(map));

    free(map);
    return 0;
}