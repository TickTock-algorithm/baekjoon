/**2048 (Easy)
 * https://www.acmicpc.net/problem/12100
 * 1 초 	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int N;

int move(int *board, int rc, int se) {
	int step = (se ? 1 : -1) * (rc ? 1 : N);
	int *p, last, num, *std;
	int max = 0;
	for (int i = 0; i < N; i++) {
		std = p = (board + i * (rc ? N : 1) + (se ? 0 : -step * (N - 1)));
		last = num = 0;
		for (int g = 0; g < N; g++) {
			num = *(std + g * step);
			if (num) {
				if (last == num) {
					*p = last + num;
					max = max > *p ? max : *p;
					p += step;
					num = 0;
				} else if (last) {
					*p = last;
					p += step;
				}
				last = num;
				num = 0;
			}
		}
		if (last)
			*p = last;
		else
			*p = 0;

		for (int *g = std + (N - 1) * step; g != p; g -= step)
			* g = 0;
	}
	return max;
}

int func(int *board, int max, int step) {
	if (step-- > 0) {
		int *tmp_board = (int*)malloc(sizeof(int) * N * N);
		for (int i = 0; i < 4; i++) {
			memcpy(tmp_board, board, sizeof(int)*N * N);
			int tmp = func(tmp_board, move(tmp_board, i / 2, i % 2), step);
			max = max > tmp ? max : tmp;
		}
		free(tmp_board);
	}
	return max;
}

int main() {
	scanf("%d", &N);
	int *board;
	int max = 0;
	board = (int*)malloc(sizeof(int) * N * N);
	for (int i = 0; i < N * N; i++) {
		scanf("%d", board + i);
		max = max > *(board + i) ? max : *(board + i);
	}

	printf("%d\n", func(board, max, 5));
}