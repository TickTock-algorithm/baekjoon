/**뱀
 * https://www.acmicpc.net/problem/3190
 * 1 초 	128 MB
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RIGHT 1
#define UP -N
#define LEFT -1
#define DOWN N
#define APPLE 101

int N, K, L;
int *map, *tail, *head;

int main() {
    scanf("%d %d", &N, &K);
    map = (int *)malloc(sizeof(int) * N * N);
    memset(map, 0, sizeof(int) * N * N);
    tail = head = map;
    *head = RIGHT;
    for (int i = 0; i < K; i++) {
        int r, c;
        scanf("%d %d", &r, &c);
        *(map + (r - 1) * N + (c - 1)) = APPLE;
    }

    scanf("%d", &L);
    int sec = 0;
    for (int i = 0;; i++) {
        int time = -1;
        char rot = 0;
        if (i < L) scanf("%d %c", &time, &rot);
        while ((sec < time) || (time < 0)) {
            sec++;
            int tmp;
            tmp = *head;
            int n = ((long)head - (long)map) / sizeof(int);

            if ((tmp == RIGHT && n % N == N - 1) ||  //벽에 부딪힘
                (tmp == LEFT && n % N == 0) || (tmp == UP && n / N == 0) ||
                (tmp == DOWN && n / N == N - 1)) {
                printf("%d\n", sec);
                return 0;
            }

            head += *head;

            switch (*head) {
                case 0:
                    *head = tmp;
                    tmp = *tail;
                    *tail = 0;
                    tail += tmp;
                    break;
                case APPLE:
                    *head = tmp;
                    break;
                default:  //몸에 부딪힘
                    printf("%d\n", sec);
                    return 0;
            }
        };
        if (rot)
            if (rot == 'L')  //회전
                *head = (*head % N) ? *head * -N : *head / N;
            else
                *head = (*head % N) ? *head * N : *head / -N;
    }
    printf("%d\n", sec);
}