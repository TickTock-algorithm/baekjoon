/**연산자 끼워넣기
 * https://www.acmicpc.net/problem/14888
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int N, *nums;
int min, max;

int calc(int pos, int oper[4], int result) {
    if (pos == N) return result;
    int res;
    for (int i = 0; i < 4; i++) {
        if (oper[i] > 0) {
            oper[i]--;
            switch (i) {
                case 0:  // +
                    res = calc(pos + 1, oper, result + nums[pos]);
                    break;
                case 1:  // -
                    res = calc(pos + 1, oper, result - nums[pos]);
                    break;
                case 2:  // *
                    res = calc(pos + 1, oper, result * nums[pos]);
                    break;
                case 3:  // /
                    res = calc(pos + 1, oper, result / nums[pos]);
                    break;
            }
            oper[i]++;
            min = min > res ? res : min;
            max = max < res ? res : max;
        }
    }
    return res;
}

int main() {
    scanf("%d", &N);

    nums = (int*)malloc(N * sizeof(int));
    for (int i = 0; i < N; i++) scanf("%d", nums + i);

    int oper[4];
    for (int i = 0; i < 4; i++) scanf("%d", oper + i);

    min = ~0u >> 1;  // INT_MAX
    max = ~0 ^ min;  // INT_MIN

    calc(1, oper, *nums);

    printf("%d\n%d\n", max, min);
}