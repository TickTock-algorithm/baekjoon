/**나무 재테크
 * https://www.acmicpc.net/problem/16235
 * 1 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<deque>
#include<algorithm>

using namespace std;

struct cell{
    int energy;
    int refill;
    deque<int> *tree;
};

int N, M, K;

void spring_summer(struct cell* map){
    for(int i=0; i<N*N; i++){
        
        int len=map[i].tree->size();
        int n=0;
        for(n=0; n<len; n++){
            int tree=map[i].tree->front();
            if(map[i].energy>=tree){
                map[i].energy-=tree;
                map[i].tree->pop_front();
                map[i].tree->push_back(tree+1);
            }else
                break;
        }
        for(; n<len; n++){
            map[i].energy+=map[i].tree->front()/2;
            map[i].tree->pop_front();
        }
    }
}

void fall_winter(struct cell* map){
    for(int i=0; i<N*N; i++){
        for(int g=0, len=map[i].tree->size(); g<len; g++){
            int r=i/N, c=i%N;
            int tree=map[i].tree->at(g);
            if(tree%5==0){
                if(r>0) map[i-N].tree->push_front(1);
                if(r<N-1) map[i+N].tree->push_front(1);
                if(c>0) map[i-1].tree->push_front(1);
                if(c<N-1) map[i+1].tree->push_front(1);
                if(r>0 && c>0) map[i-N-1].tree->push_front(1);
                if(r<N-1 && c<N-1) map[i+N+1].tree->push_front(1);
                if(c>0 && r<N-1) map[i-1+N].tree->push_front(1);
                if(c<N-1 && r>0) map[i+1-N].tree->push_front(1);
            }
        }
        map[i].energy+=map[i].refill;
    }
}

int solve(struct cell* map){
    for(int i=0; i<K; i++){
        spring_summer(map);
        fall_winter(map);
    }

    int sum=0;
    for(int i=0; i<N*N; i++)
        sum+=map[i].tree->size();
    return sum;
}

int main(){
    scanf("%d %d %d", &N, &M, &K);
    
    struct cell *map=(struct cell*)malloc(N*N*sizeof(struct cell));
    for(int i=0; i<N*N; i++){
        scanf("%d", &map[i].refill);
        map[i].energy=5;
        map[i].tree=new deque<int>;
    }
    
    int r, c, a;
    for(int i=0; i<M; i++){
        scanf("%d %d %d", &r, &c, &a);
        map[(r-1)*N+(c-1)].tree->push_back(a);
    }
    for(int i=0; i<N*N; i++)
        sort(map[i].tree->begin(), map[i].tree->end());
    
    printf("%d\n", solve(map));

    for(int i=0; i<N*N; i++){
        delete map[i].tree;
    }
    free(map);
}