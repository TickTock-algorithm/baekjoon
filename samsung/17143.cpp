/**낚시왕
 * https://www.acmicpc.net/problem/17143
 * 1 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

using namespace std;

int D[]={0, 0, 0, 1, -1};

int R, C, M;

struct shark{
    int r, c;
    int s, d, z;
};

void printMap(struct shark *shark){
    int *map=(int*)malloc(R*C*sizeof(int));
    memset(map, 0, R*C*sizeof(int));

    for(int i=0; i<M; i++){
        if(shark[i].z<0)
            continue;
        map[(shark[i].r-1)*C+shark[i].c-1]=shark[i].z;
    }

    for(int r=0; r<R; r++){
        for(int c=0; c<C; c++)
            printf("%d ", map[r*C+c]);
        puts("");
    }
    puts("");
    free(map);
}

void moveShark(struct shark* shark){
    struct shark **map=(struct shark**)malloc(R*C*sizeof(struct shark*));
    memset(map, 0, R*C*sizeof(struct shark*));
    
    for(int i=0; i<M; i++){
        if(shark[i].z<0)
            continue;
        int *m=(shark[i].d%C?&shark[i].c:&shark[i].r);
        int limit=(shark[i].d%C?C:R);
        
        for(int g=0; g<shark[i].s; g++){
            *m+=shark[i].d>0?1:-1;
            if(*m<=0){
                shark[i].d*=-1;
                *m=2;
            }else if(*m>limit){
                shark[i].d*=-1;
                *m=limit-1;
            }
        }
        struct shark **p=&map[(shark[i].r-1)*C+shark[i].c-1];
        if(*p==NULL){
            *p=shark+i;
        }else if((*p)->z>shark[i].z){
            shark[i].z=-1;
        }else{
            (*p)->z=-1;
            *p=shark+i;
        }
    }
    free(map);
}

int fishing(struct shark* shark, int col){
    struct shark* target=NULL;
    for(int i=0; i<M; i++){
        if(shark[i].z<0)
            continue;
        if(shark[i].c==col)
            if(target==NULL || target->r>shark[i].r)
                target=shark+i;
    }
    if(target==NULL)
        return 0;
    int ret=target->z;
    target->z=-1;
    return ret;
}

int main(){
    scanf("%d %d %d", &R, &C, &M);
    D[1]=-C;
    D[2]=C;
    struct shark *shark=(struct shark*)malloc(M*sizeof(struct shark));

    for(int i=0; i<M; i++){
        scanf("%d %d %d %d %d", &shark[i].r, &shark[i].c, &shark[i].s, &shark[i].d, &shark[i].z);
        shark[i].d=D[shark[i].d];
        shark[i].s=shark[i].s%((((shark[i].d%C)?C:R)-1)*2);
    }

    int result=0;
    for(int i=1; i<=C; i++){
        result+=fishing(shark, i);
        moveShark(shark);
        // printMap(shark);
    }
    printf("%d\n", result);
    free(shark);
    return 0;
}