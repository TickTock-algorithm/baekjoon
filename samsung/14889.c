/**스타트와 링크
 * https://www.acmicpc.net/problem/14889
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int N, *score;

int calc(int num, int result, int team, int count) {
    if (count == N / 2 && num == N) return result < 0 ? -result : result;
    if (num - N / 2 > count || count > N / 2)  //반씩 나눠지지 않음
        return ~0u >> 1;

    int tmp1 = 0, tmp2 = 0;
    for (int i = 0; i < num; i++)
        if (team & (1 << i))
            tmp1 += *(score + i * N + num);
        else
            tmp2 += *(score + i * N + num);

    int res2 = calc(num + 1, result - tmp2, team, count);
    if (!res2) return 0;
    team |= (1 << num);
    int res1 = calc(num + 1, result + tmp1, team, count + 1);
    return !res2 ? 0 : (res1 < res2 ? res1 : res2);
}

int main() {
    scanf("%d", &N);
    score = (int*)malloc(N * N * sizeof(int));
    for (int i = 0; i < N * N; i++) scanf("%d", score + i);

    for (int i = 0; i < N; i++)
        for (int g = i + 1; g < N; g++)
            *(score + i * N + g) += *(score + g * N + i);

    int result = calc(0, 0, 1, 1);
    printf("%d\n", result);
    return 0;
}