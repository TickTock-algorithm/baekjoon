/**아기 상어
 * https://www.acmicpc.net/problem/16236
 * 2 초	512 MB
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<deque>
#include<algorithm>

int N;
struct pos{
    int r, c;
};
struct shark{
    int size;
    int ate;
} shark;
std::deque<struct pos*> step;

void newStep(int* map, int r, int c){
    int *p=map+r*N+c;
    if(*p<0 || *p>shark.size)
        return;
    *p=*p?*p:-1;

    struct pos *pos=(struct pos*)malloc(sizeof(struct pos));
    pos->r=r;
    pos->c=c;
    step.push_back(pos);
}
int* find(int* map, int r, int c){
    int *p=map+r*N+c;

    if(*p>0 && *p<shark.size)
        return p;
    *p=-1;
    
    if(r>0) newStep(map, r-1, c);
    if(r<N-1) newStep(map, r+1, c);
    if(c>0) newStep(map, r, c-1);
    if(c<N-1) newStep(map, r, c+1);
    return NULL;
}
bool comp(struct pos *a, struct pos *b){
    if(a->r!=b->r)
        return a->r < b->r;
    return a->c < b->c;
}
int solve(int* map){
    std::deque<struct pos*> fish;
    int time=0;
    int *copy_map=(int*)malloc(N*N*sizeof(int));
    memcpy(copy_map, map, N*N*sizeof(int));
    for(int t=0; ; t++){
        int len=step.size();
        if(!len)
            break;
        
        for(int i=0; i<len; i++){
            struct pos *pos=step.front();
            if(find(copy_map, pos->r, pos->c))
                fish.push_back(pos);
            else{
                free(pos);
            }
            step.pop_front();
        }

        len=fish.size();
        
        if(len){
            sort(fish.begin(), fish.end(), comp);
            struct pos *pos=fish.front();
            if(++shark.ate>=shark.size){
                shark.ate=0;
                shark.size++;
            }

            map[pos->r*N+pos->c]=0;
            memcpy(copy_map, map, N*N*sizeof(int));
            fish.pop_front();
            time=t--;

            for(int i=1; i<len; i++){
                free(fish.front());
                fish.pop_front();
            }
            for(int i=0, len=step.size(); i<len; i++){
                free(step.front());
                step.pop_front();
            }
            step.push_back(pos);
        }
    }
    free(copy_map);
    return time;
}
int main(){
    scanf("%d", &N);
    int *map=(int*)malloc(N*N*sizeof(int));
    
    int r,c;
    struct pos *init_pos=(struct pos*)malloc(sizeof(struct pos));
    for(int i=0; i<N*N; i++){
        scanf("%d", map+i);
        if(map[i]==9){
            init_pos->r=i/N;
            init_pos->c=i%N;
            step.push_back(init_pos);
            map[i]=0;
        }
    }
    shark.size=2;
    shark.ate=0;

    printf("%d\n", solve(map));
    free(map);
    return 0;
}