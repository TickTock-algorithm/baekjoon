/**시험 감독
 * https://www.acmicpc.net/problem/13458
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
    int N, *A, B, C;
    scanf("%d", &N);
    A = (int*)malloc(sizeof(int) * N);
    for (int i = 0; i < N; i++) scanf("%d", A + i);
    scanf("%d %d", &B, &C);

    long long result = N;
    for (int i = 0; i < N; i++) {
        *(A + i) -= B;
        if (*(A + i) > 0) result += *(A + i) / C + ((*(A + i) % C) ? 1 : 0);
    }
    printf("%ld\n", result);
}