/**로봇 청소기
 * https://www.acmicpc.net/problem/14503
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int N, M, *map;

int main() {
    scanf("%d %d", &N, &M);
    int r, c, d;
    scanf("%d %d %d", &r, &c, &d);
    map = (int *)malloc(sizeof(int) * N * M);
    for (int i = 0; i < N * M; i++) scanf("%d", map + i);

    int rotation_count = 0;
    int *p = map + r * M + c;
    int move = d % 2 ? 1 : M;  // 청소기가 바라보고 있는 방향으로 가는 길
    move *= d && d < 3 ? 1 : -1;
    int result = 0;

    while (1) {
        if (!*p) {  // 1
            *p = -1;
            result++;
            rotation_count = 0;
        }
        if (rotation_count < 4) {  // 2.1, 2.2
            move = (d % 2) ? (move * -M) : (move / M);
            d = (d - 1 + 4) % 4;
            if (!*(p + move)) {  // 2.1
                p += move;
                rotation_count = 0;
            } else
                rotation_count++;
        } else {                  // 2.3, 2.4
            if (*(p - move) > 0)  // 2.4
                break;
            else
                p -= move;
            rotation_count = 0;
        }
    }
    printf("%d\n", result);
}