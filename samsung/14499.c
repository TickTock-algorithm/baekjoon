/**주사위 굴리기
 * https://www.acmicpc.net/problem/14499
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int N, M;
struct dice {
    int num[6];
    int pos[2];
};
int *map;

int main() {
    scanf("%d %d", &N, &M);
    int oper_count, oper;
    struct dice dice;
    memset(&dice.num, 0, sizeof(int) * 6);
    scanf("%d %d %d", dice.pos, dice.pos + 1, &oper_count);
    map = (int *)malloc(sizeof(int) * N * M);

    for (int i = 0; i < N * M; i++) scanf("%d", map + i);
    for (int i = 0; i < oper_count; i++) {
        scanf("%d", &oper);

        dice.pos[oper < 3 ? 1 : 0] += (oper / 2 != 1) ? 1 : -1;
        if (dice.pos[0] < 0 || dice.pos[0] >= N || dice.pos[1] < 0 ||
            dice.pos[1] >= M) {
            dice.pos[oper < 3 ? 1 : 0] -= (oper / 2 != 1) ? 1 : -1;
            continue;
        }

        int tmp = dice.num[oper];
        dice.num[oper] = dice.num[5];
        dice.num[5] = dice.num[oper + (oper % 2 ? 1 : -1)];
        dice.num[oper + (oper % 2 ? 1 : -1)] = dice.num[0];
        dice.num[0] = tmp;

        int *p = map + dice.pos[0] * M + dice.pos[1];
        if (*p) {
            dice.num[0] = *p;
            *p = 0;
        } else {
            *p = dice.num[0];
        }
        printf("%d\n", dice.num[5]);
    }
}