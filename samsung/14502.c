/**연구소
 * https://www.acmicpc.net/problem/14502
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_WALL 3

int N, M;
int *map, *buf_map;

int count_space(int *map) {
    int count = 0;
    for (int i = 0; i < N * M; i++)
        if (!*(map + i)) count++;
    return count;
}

void infect(int *map, int x, int y) {
    for (int y = 0; y < N; y++)
        for (int x = 0; x < M; x++) {
            int *p = map + y * M + x;
            if (*p == 2) {
                int back = 0;
                if (x != 0 && *(p - 1) == 0) {
                    *(p - 1) = 2;
                    back = 1;
                }
                if (y != 0 && *(p - M) == 0) {
                    *(p - M) = 2;
                    back = M;
                }
                if (y != N - 1 && *(p + M) == 0) *(p + M) = 2;
                if (x != M - 1 && *(p + 1) == 0) *(p + 1) = 2;
                if (back) {
                    y -= back / M;
                    x -= back % M + 1;
                }
            }
        }
}

int build(int pos, int count) {
    int res, max = 0;
    if (count >= MAX_WALL) {
        memcpy(buf_map, map, sizeof(int) * M * N);
        infect(buf_map, 0, 0);
        return count_space(buf_map);
    }
    if (pos >= N * M - 1) return 0;
    for (int i = pos; i < N * M; i++)
        if (!*(map + i)) {
            *(map + i) = 1;
            res = build(i + 1, count + 1);
            max = max < res ? res : max;
            *(map + i) = 0;
        }
    return max;
}

int main() {
    scanf("%d %d", &N, &M);
    map = (int *)malloc(sizeof(int) * N * M);
    for (int i = 0; i < N * M; i++) scanf("%d", map + i);

    buf_map = (int *)malloc(sizeof(int) * N * M);
    printf("%d\n", build(0, 0));
}