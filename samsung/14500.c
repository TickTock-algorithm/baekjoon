/**테트로미노
 * https://www.acmicpc.net/problem/14500
 * 2 초 	512 MB
 */

#include <stdio.h>
#include <stdlib.h>

int N, M, *map;
struct pos {
    int x;
    int y;
};
struct pos tetro[][4] = {
    {{0, 0}, {1, 0}, {2, 0}, {3, 0}}, {{0, 0}, {1, 0}, {0, 1}, {1, 1}},
    {{0, 0}, {0, 1}, {0, 2}, {1, 2}}, {{0, 0}, {0, 1}, {1, 1}, {1, 2}},
    {{0, 0}, {1, 0}, {2, 0}, {1, 1}}, {{0, 0}, {1, 0}, {0, 1}, {0, 2}},
    {{1, 0}, {0, 1}, {1, 1}, {2, 1}}, {{1, 0}, {1, 1}, {1, 2}, {0, 2}},
    {{0, 0}, {1, 0}, {1, 1}, {1, 2}}, {{1, 0}, {0, 1}, {1, 1}, {0, 2}}};

int main() {
    scanf("%d %d", &N, &M);
    map = (int *)malloc(sizeof(int) * N * M);
    for (int i = 0; i < N * M; i++) scanf("%d", map + i);

    int max = 0;
    for (int i = 0; i < sizeof(tetro) / 4 / sizeof(struct pos); i++) {
        for (int r = 0; r < N; r++)
            for (int c = 0; c < M; c++) {
                int *p = map + r * M + c;
                int res1 = 0, res2 = 0;
                for (int g = 0; g < 4; g++) {
                    if ((tetro[i][g].x + c < M) &&
                        (tetro[i][g].y + r < N))
                        res1 += *(p + tetro[i][g].y * M + tetro[i][g].x);
                    if ((tetro[i][g].y + c < M) &&
                        (tetro[i][g].x + r < N))
                        res2 += *(p + tetro[i][g].x * M + tetro[i][g].y);
                }
                max = max < res1 ? res1 : max;
                max = max < res2 ? res2 : max;
            }
    }

    printf("%d\n", max);
}