/**인구 이동
 * https://www.acmicpc.net/problem/16234
 * 2 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#include<vector>

int N, L, R;

struct info{
    int total;
    int count;
};

int check(int *p1, int *p2){
    int t=abs(*p1-*p2);
    if(t>=L && t<=R)
        return 1;
    return 0;
}
int group(int *map, struct info **info, int r, int c){
    int count=1;
    int *mp=map+r*N+c;
    struct info **ip=info+r*N+c;
    (**ip).total+=*mp;
    (**ip).count++;

    if(r>0 && !*(ip-N) && check(mp, mp-N)){
        *(ip-N)=*ip;
        count+=group(map, info, r-1, c);
    }
    if(c>0 && !*(ip-1) && check(mp, mp-1)){
        *(ip-1)=*ip;
        count+=group(map, info , r, c-1);
    }
    if(r<N-1 && !*(ip+N) && check(mp, mp+N)){
        *(ip+N)=*ip;
        count+=group(map, info, r+1, c);
    }
    if(c<N-1 && !*(ip+1) && check(mp, mp+1)){
        *(ip+1)=*ip;
        count+=group(map, info, r, c+1);
    }
    return count;
}

int solve(int *map){
    int count=0;
    struct info **info=(struct info**)malloc(N*N*sizeof(struct info*));
    
    int flag=0;
    std::vector<struct info*> infos;
    do{
        memset(info, 0, N*N*sizeof(struct info*));
        flag=0;
        for(int r=0; r<N; r++){
            for(int c=0; c<N; c++){
                struct info **ip=info+r*N+c;
                if(*ip==NULL){
                    *ip=(struct info*)malloc(sizeof(struct info));
                    memset(*ip, 0, sizeof(struct info));
                    if(group(map, info, r, c)>1)
                        infos.push_back(*ip);
                    else{
                        free(*ip);
                        *ip=NULL;
                    }
                }
            }
        }
        for(int r=0; r<N; r++){
            for(int c=0; c<N; c++){
                struct info *ip=info[r*N+c];
                if(ip!=NULL && ip->count>1){
                    flag=1;
                    map[r*N+c]=ip->total/ip->count;
                }
            }
        }
        for(int i=0; i<infos.size(); i++)
            free(infos[i]);
        infos.clear();
        count++;
    }while(flag);
    
    free(info);
    return count-1;
}

int main(){
    scanf("%d %d %d", &N, &L, &R);
    int *map=(int*)malloc(N*N*sizeof(int));
    for(int i=0; i<N*N; i++)
        scanf("%d", map+i);
    
    printf("%d\n", solve(map));

    free(map);
    return 0;
}