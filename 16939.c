/**2×2×2 큐브
 * https://www.acmicpc.net/problem/16939
 * 2 초	512 MB
 */

#include<stdio.h>
#include<string.h>

int DECAL[6]={2, 5, 0, 4, 3, 1};
int SIDE[]={0, 1, 3};
int MOVE[][8]={
    {13, 14, 5, 6, 17, 18, 21, 22},
    {3, 4, 17, 19, 10, 9, 16, 14},
    {0,},
    {1, 3, 5, 7, 9, 11, 24, 22},
};
void rotate(int cube[6][4], int s){
    int buf[24];
    memcpy(buf, cube, 6*4*sizeof(int));
    for(int i=0; i<8; i++)
        *((int*)cube+MOVE[s][(i+2)%8]-1)=*(buf+MOVE[s][i]-1);
}
int checkSide(int cube[6][4], int s){
    int c=cube[s][0];
    for(int i=1; i<4; i++)
        if(c!=cube[s][i])
            return 0;
    return 1;
}
int checkSolved(int cube[6][4]){
    for(int i=0; i<3; i++)
        if(!checkSide(cube, SIDE[i]) || !checkSide(cube, DECAL[SIDE[i]]))
            return 0;
    return 1;
}
int solve(int cube[6][4]){
    for(int i=0; i<3; i++){
        int s=SIDE[i];
        if(checkSide(cube, s) && checkSide(cube, DECAL[s])){
            for(int t=0; t<3; t++){
                rotate(cube, s);
                if(checkSolved(cube))
                    return 1;
                rotate(cube, s);
                rotate(cube, s);
                return checkSolved(cube);                
            }
        }
    }
    return 0;
}

int main(){
    int cube[6][4];
    for(int i=0; i<24; i++)
        scanf("%d", &cube[i/4][i%4]);
    printf("%d\n", solve(cube));
    return 0;
}