/**스티커 붙이기
 * https://www.acmicpc.net/problem/18808
 * 2 초	512 MB
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct sticker{
    int r, c;
    int *map;
    int size;
};

int N, M, K;

void rotate(struct sticker* s){
    int *map=(int*)malloc(sizeof(int)*s->r*s->c);
    for(int r=0; r<s->r; r++){
        for(int c=0; c<s->c; c++){
            map[c*s->r+(s->r-r-1)]=s->map[r*s->c+c];
        }
    }
    s->r^=s->c^=s->r^=s->c;
    free(s->map);
    s->map=map;
}

int attach(int* map, struct sticker* s){
    for(int r=0; r<s->r; r++){
        for(int c=0; c<s->c; c++){
            if(map[r*M+c] && s->map[r*s->c+c])
                return 1;
        }
    }
    for(int r=0; r<s->r; r++){
        for(int c=0; c<s->c; c++){
            map[r*M+c]+=s->map[r*s->c+c];
        }
    }
    return 0;
}

int find(int* map, struct sticker* s){
    for(int rot=0; rot<4; rot++){
        for(int r=0; r<N-s->r+1; r++){
            for(int c=0; c<M-s->c+1; c++){
                if(!attach(map+r*M+c, s)){
                    return 0;
                }
            }
        }
        rotate(s);
    }
    return 1;
}

int solve(struct sticker *sticker){
    int *map=(int*)malloc(sizeof(int)*N*M);
    memset(map, 0, sizeof(int)*N*M);

    int count=0;
    for(int k=0; k<K; k++){
        if(!find(map, sticker+k)){
            count+=sticker[k].size;
        }
    }
    
    free(map);
    return count;
}

int main(){
    scanf("%d %d %d", &N, &M, &K);
    struct sticker *sticker=(struct sticker*)malloc(sizeof(struct sticker)*K);
    for(int k=0; k<K; k++){
        struct sticker *s=sticker+k;
        scanf("%d %d", &s->r, &s->c);
        s->map=(int*)malloc(sizeof(int)*s->r*s->c);
        for(int i=0, len=s->r*s->c; i<len; i++){
            scanf("%d", s->map+i);
            s->size+=s->map[i];
        }
    }

    printf("%d\n", solve(sticker));

    for(int k=0; k<K; k++)
        free(sticker[k].map);
    free(sticker);
    return 0;
}